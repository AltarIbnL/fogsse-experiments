#include "contiki.h"
#include "rpl.h"
#include "httpd-simple.h"
#include "myencrypt.h"
#include "sha256.h"
#include "test_uti.h"
#include "base64.h"


#include <clock.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "Web Sense"
#define LOG_LEVEL LOG_LEVEL_INFO


int SHA_256(char *msg, uint8_t *hashtag)
{

  struct tc_sha256_state_struct sh;
  (void)tc_sha256_init(&sh);
  tc_sha256_update(&sh, (const uint8_t *) msg, strlen(msg));
  (void)tc_sha256_final(hashtag, &sh);

  return 0;       
}


/*---------------------------------------------------------------------------*/
static
PT_THREAD(generate_routes(struct httpd_state *s))
{
  char msgstring[100];

  const uint8_t aeskey[16] = {0xa4, 0xd6, 0x61, 0x6b, 0xd0, 0x4f, 0x87, 0x33, 0x5b, 0x0e, 0x53, 0x35, 0x12, 0x27, 0xa9, 0xee};
  
  struct tc_aes_key_sched_struct enc;
  tc_aes128_set_encrypt_key(&enc, aeskey);

  PSOCK_BEGIN(&s->sout);
  
  //Generate dummy temperature figures
  int temperature = 10 + rand() % 25;
  char temp[10];
  sprintf(temp, "%d", temperature);
  printf("Temperature: %d \n", temperature);

  //Generate the filename (nodeID + measurement category + Timestamp)
  /* Node IDS 
  USB1 = ZOL-RM02-B1002741
  USB2 = ZOL-RM02-B1002661 
  USB0 (Router Node) = ZOL-BO004-B100025
  Change according to Node being used
  */
  const char *nodeID = "ZOL-BO004-B100025";
  clock_time_t time_stamp = clock_time();
  const char *msgcat = "Temperature";
  
  //Create the filename data
  char filename[32];
  sprintf(filename, "%s, %lu, %s", nodeID, time_stamp, msgcat);
  printf("Filename: %s\n", filename);

  //Begin timestamp to measure the beginning of the functions
  clock_time_t start_time = clock_time();


  //Hashing the keyword values (keyword hashing)
  uint8_t hash_temperature[32];
  SHA_256(temp, hash_temperature);
  show_str("Hashed Temperature:" ,hash_temperature, sizeof(hash_temperature));

  //Encoding the hashed keyword to base64 for sending
  char *hash_temperature_base64;
  hash_temperature_base64 = b64_encode(hash_temperature, sizeof(hash_temperature));
  printf("Encoded Hashed Temperature = %s\n", hash_temperature_base64);


  //Encrypt the contents of the filename data
  uint8_t filename_cipher[16];
  uint8_t filename_hash[32];

  SHA_256(filename, filename_hash);
  show_str("Hashed Message: ", filename_hash, sizeof(filename_hash));
  (void)tc_aes_encrypt(filename_cipher, filename_hash, &enc);
  show_str("Encrypted Message Cipher: ", filename_cipher, sizeof(filename_cipher));


  //Encoding the encrypted filename data
  char *filename_base64;
  filename_base64 = b64_encode(filename_cipher, sizeof(filename_cipher));
  printf("Encoded Encrypted Filename = %s\n", filename_base64);


  //Generate final string to be sent to the middleware
  //Final message = [Hash of keyword + Encrypted Filename Contents]
  sprintf(msgstring,"{\"Temperature\":\"%s\",\"Filename\":\"%s\"}", hash_temperature_base64, filename_base64);

  //Ending timestamp to measure the end of the functions
  clock_time_t end_time = clock_time();

  //Calculate the time taken to generate the search token
  unsigned long time_taken = end_time - start_time;
  printf("Time Taken to create Add Token: %lu ticks\n", time_taken);

  printf("Message being sent: %s\n", msgstring);
  printf("Sending values to Middleware .......... \n");

  printf("\n\n\n");
  free(filename_base64);
  free(hash_temperature_base64);

  //Send final message to middleware for further computation
  SEND_STRING(&s->sout, msgstring);
  PSOCK_END(&s->sout);
}

/*---------------------------------------------------------------------------*/
PROCESS(webserver_nogui_process, "Web Sense server");
PROCESS_THREAD(webserver_nogui_process, ev, data)
{
  PROCESS_BEGIN();

  httpd_init();

  while(1) 
  {
    PROCESS_WAIT_EVENT_UNTIL(ev == tcpip_event);
    httpd_appcall(data);
  }

  PROCESS_END();
}

/*---------------------------------------------------------------------------*/
httpd_simple_script_t
httpd_simple_get_script(const char *name)
{
  return generate_routes;
}


/*---------------------------------------------------------------------------*/
/* Declare and auto-start this file's process */
PROCESS(web_sense, "Web Sense");
AUTOSTART_PROCESSES(&web_sense);

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(web_sense, ev, data)
{
  PROCESS_BEGIN();

  PROCESS_NAME(webserver_nogui_process);
  process_start(&webserver_nogui_process, NULL);

  LOG_INFO("SSE Computation Initialized started\n");

  PROCESS_END();
}